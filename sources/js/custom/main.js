(function(){
	var app = {
		module: {

			iosFix: {
				init: function(){

					if ( !!Modernizr ){

						Modernizr.addTest('ipad', function () {
							return !!navigator.userAgent.match(/iPad/i);
						}).addTest('iphone', function () {
							return !!navigator.userAgent.match(/iPhone/i);
						}).addTest('ipod', function () {
							return !!navigator.userAgent.match(/iPod/i);
						}).addTest('ios', function () {
							return (Modernizr.ipad || Modernizr.ipod || Modernizr.iphone);
						});
					}

					var $w = $(window), w = $w.width() <= 1360 ? 1080 : false ;

					if (Modernizr.touchevents && w){
						$('meta[name=viewport]').remove();
						$('head').append('<meta name="viewport" content="width=' + w + ', user-scalable=0">');
					}


					/*if (/iPhone/.test(navigator.userAgent) && !window.MSStream){
						$d.on("focus", "input, textarea, select", function()
						{
							$('meta[name=viewport]').remove();
							$('head').append('<meta name="viewport" content="width=' + w + ', user-scalable=0">');
						}).on("blur", "input, textarea, select", function()
						{
							$('meta[name=viewport]').remove();
							$('head').append('<meta name="viewport" content="width=device-width">');
						});
					}*/
				}
			},

			checkForm: function(form){
				var $el = $(form),
					wrong = false;

				$el.find('[data-required]').each(function(){
					var $t = $(this),
						type = $t.data('required'),
						$wrap = $t.closest('.form-field'),
						val = $.trim($t.val()),
						rexp = /.+$/igm;

					$wrap.removeClass('error').removeClass('success');

					switch (type) {
						case 'number':
							rexp = /^\d+$/i;
							break;
						case 'phone':
							rexp = /[\+]\d\s[\(]\d{3}[\)]\s\d{3}\s\d{2}\s\d{2}/i;
							break;
						case 'letter':
							rexp = /^[A-zА-яЁё]+$/i;
							break;
						case 'rus':
							rexp = /^[А-яЁё]+$/i;
							break;
						case 'email':
							rexp = /^[-._a-z0-9]+@(?:[a-z0-9][-a-z0-9]+\.)+[a-z]{2,6}$/i;
							break;
						case 'password':
							rexp = /^(?=^.{8,}$)(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).*$/;
							break;
						default:
							rexp = /.+$/igm;
					}

					if ( !rexp.test(val) || val == 'false' || !val ){
						wrong = true;
						$wrap.addClass('error');

					} else {
						$wrap.addClass('success');
					}

				});

				return !wrong;

			},

			calculator: {

				storage: {
					gender: null,
					effect: null,
					age: null,
					height: null,
					weight: null,
					activity: null
				},

				check: function(){

					var wrong = false, key, res,
						$steps = $('.calculate-step'),
						$step1 = $('.calculate-step_1'),
						$step2 = $('.calculate-step_2'),
						$step3 = $('.calculate-step_3'),
						$step4 = $('.calculate-step_4'),
						$result = $('.calculate-result');

					for (key in self.calculator.storage) {
						if ( self.calculator.storage[key] == null ) wrong = true;
					}

					if ( self.calculator.storage.gender == null ){ $step2.addClass('disable'); } else { $steps.removeClass('active'); $step2.removeClass('disable').addClass('active'); }
					if ( self.calculator.storage.effect == null ){ $step3.addClass('disable'); } else { $steps.removeClass('active'); $step3.removeClass('disable').addClass('active'); $step3.find('input').prop('disabled', false); }
					if ( !self.calculator.storage.age || !self.calculator.storage.height || !self.calculator.storage.weight ){ $step4.addClass('disable'); } else { $steps.removeClass('active'); $step4.removeClass('disable').addClass('active'); }
					if ( !wrong ){
						$result.removeClass('disable');
						res = ( (self.calculator.storage.gender == "m" ? (+5) : (-161)) + 9.99 * self.calculator.storage.weight + 6.25 * self.calculator.storage.height - 4.92 * self.calculator.storage.age) * parseFloat( self.calculator.storage.activity ) ;
						res = Math.round(res + res * parseFloat(self.calculator.storage.effect));
						$result.find('.js-calculate-result').html(res + ' ккал/день');
					} else {
						$result.addClass('disable').find('.js-calculate-result').html('');
					}

					//console.info(wrong);
					//console.info(self.calculator.storage);
				},

				init: function(){

					$('body').on('mousedown', '.disable', function(e) {
						e.preventDefault();
						e.stopPropagation();
					}).on('keyup input', '.js-input-calculate', function() {
						var $t = $(this);
						var max = $t.attr('max');
						var min = $t.attr('min');

						if ( $t.val().match(/[^0-9]/g) ) {
							var _newVal = $t.val().replace(/[^0-9]/g, '');
							$t.val(_newVal);
						}

						self.calculator.storage[$t.attr('name')] = +$t.val();
						self.calculator.check();

					}).on('change', '.js-input-calculate', function() {
						var $t = $(this);
						var max = $t.attr('max');
						var min = $t.attr('min');

						if ( +$t.val() < min ){
							$t.val(min);
						} else if (  +$t.val() > max ){
							$t.val(max);
						}

						self.calculator.storage[$t.attr('name')] = +$t.val();
						self.calculator.check();

					}).on('click', '.js-btn-calculate', function() {

						var $t = $(this);


						if ( !$t.closest('.disable').size() ) {

							$t.addClass('active')
								.siblings()
								.removeClass('active');
							self.calculator.storage[$t.data('calc')] = $t.data('value');
							self.calculator.check();
						} else {
							var $active = $('.calculate-step.active');
							$active.each(function(){
								var $t = $(this);
								$t.addClass('light');
								setTimeout(function(){ $t.removeClass('light'); }, 500);
							});
						}
					});
				}
			},

			popup: {
				create: function(popup){
					var pref = popup.indexOf('#') == 0  ? 'id' : 'class';
					var name = popup.replace(/^[\.#]/,'');
					var $popup = $('<div class="popup">'
						+			'<div class="popup-inner">'
						+				'<div class="popup-layout">'
						+					'<div class="popup-close"></div>'
						+					'<div class="popup-content"></div>'
						+				'</div>'
						+			'</div>'
						+			'<div class="popup-overlay"></div>'
						+		'</div>').appendTo('body');

					if ( pref == 'id'){
						$popup.attr(pref, name);
					} else {
						$popup.addClass(name);
					}

					return $popup;
				},

				open: function(popup, html){
					self.popup.close('.popup');
					setTimeout(function(){
						try{$.fn.fullpage.setAllowScrolling(false);} catch (e){}
					}, 10);
					var $popup = $(popup);
					if (!$popup.size()){
						$popup = self.popup.create(popup);
					}
					if( html ){
						$popup.find('.popup-content').html(html);
					}
					$('body').addClass('overflow_hidden');
					return $popup.show();
				},

				close: function(popup){
					try{$.fn.fullpage.setAllowScrolling(true);} catch (e){}
					var $popup = $(popup);
					$('body').removeClass('overflow_hidden');
					$popup.hide();
				},

				info: function(title, text, callback){
					var html = '<div class="popup-title">' + (title ? title : '') + '</div>'
						+	'<div class="popup-text">' + (text ? text : '') + '</div>'
						+	'<div class="popup-btn"><div class="btn">Ок</div></div>';
					self.popup.open('.popup_info', html);

					if ( callback ) {
						$('.popup_info').find('.btn').click(callback);
					} else {
						$('.popup_info').find('.btn').click(function(){
							self.popup.close($('.popup_info'));
						});
					}
				},

				remove: function(popup){
					var $popup = $(popup);
					$('body').removeClass('overflow_hidden');
					$popup.remove();
				}
			},

			asideMenu:{

				st: '',
				show: function (){
					self.asideMenu.st = $(window).scrollTop();
					$('html').css({'top': -self.asideMenu.st}).addClass('_menu');
				},
				hide: function (){
					$('html').removeClass('_menu').removeAttr('style');
					$('html,body').animate({scrollTop:self.asideMenu.st}, 0);
				}
			},

			feedbackSlider: {

				setCurrent: function($el){
					var $center;

					if ( self.viewPort.current == 'mobile' ) {
						$center = $el.find('.owl-item.active');
					} else {
						$center = $el.find('.owl-item.center');
					}

					var $view = $el.next('.feedback-view');

					var userPhoto = $center.find('.js-feedback-user-photo').attr('src');
					var userName = $center.find('.js-feedback-user-name').html();
					var userText = $center.find('.js-feedback-user-text').html();

					//console.info(userText);

					$view.fadeOut(300);

					setTimeout(function(){
						$view.find('.photo img').attr('src', userPhoto);
						$view.find('.name').html(userName);
						$view.find('.feedback-view__text').html(userText);
						$view.fadeIn(300);
					}, 300);
				},

				init: function(){

					$('.feedback-carousel').each(function(){

						var $t = $(this);
						var fCarousel = $t.find('.feedback-list');

						if ( !fCarousel.hasClass('owl-loaded') && fCarousel.is(':visible') ){

							var $prev = $t.find('.feedback-control__item_prev');
							var $next = $t.find('.feedback-control__item_next');

							fCarousel = $t.find('.feedback-list');
							fCarousel.owlCarousel({
								center:true,
								autoWidth:true,
								margin:0,

								mouseDrag: false,
								touchDrag: false,
								pullDrag: false,
								loop: true,
								rtl:true,
								dots: false,
								onChanged: callback,
								onRefresh: function(){
								}
							});

							var inprocess = false;

							$prev.on('click', function(){
								if ( inprocess ) return false;
								inprocess = true;
								fCarousel.trigger('prev.owl.carousel');
								setTimeout(function(){
									inprocess = false;
								},600);
							});
							$next.on('click', function(){
								if ( inprocess ) return false;
								inprocess = true;
								fCarousel.trigger('next.owl.carousel');
								setTimeout(function(){
									inprocess = false;
								},600);
							});
						}
						else if ( $t.is(':visible') ){
							//fCarousel.trigger('refresh.owl.carousel');
							//console.log(fCarousel);
						}

						function callback(){
							setTimeout(function(){
								self.feedbackSlider.setCurrent($t);
							}, 0);

						}

					});
				},

				refresh: function(){
					$('.feedback-list')
						.trigger('destroy.owl.carousel')
						.trigger('initialize.owl.carousel')
						//.removeAttr('class')
						//.addClass('feedback-list')
						//.find('.feedback-item')
						//.unwrap()
					;

					self.feedbackSlider.init();
				}
			},

			order: {

				init: function(){

					$('body')
						.on('click', '.js-btn-order', function(e) {
							var $t = $(this);
							if ($t.hasClass('disable')) return false;
							$t.addClass('active').siblings().removeClass('active');


						})
						.on('click', '.js-btn-order_cb', function(e) {
							var $t = $(this);
							if ($t.hasClass('disable')) return false;
							$t.toggleClass('active');


						})
						.on('click', '.js-order-next-step', function(e) {
							var $t = $(this);
							if ($t.hasClass('disable')) return false;
							$t.closest('.popup_order').toggleClass('step-1 step-2');

						})
						.on('click', '.js-order-next-step_2', function(e) {
							var $t = $(this);
							$t.closest('.popup_order').removeClass('step-1 step-2 step-map').addClass('step-2');
						})
						.on('click', '.js-order-show-map', function(e) {
							var $t = $(this);
							$t.closest('.popup_order').removeClass('step-1 step-2').addClass('step-map');
						})
					;
					
					$('form')
						.on('change', 'input[type=text], textarea', function(e) {
							var $t = $(this);

							if ( $t.val().length ){
								$t.closest('.form-field').addClass('filled');
							} else {
								$t.closest('.form-field').removeClass('filled');
							}
						})
						.on('change', '.js-input-full-address', function(e) {
							var $t = $(this);

							if ( $t.val().length ){
								$('.order-field_address').slideDown(300);
							}
						});
				}
			},

			viewPort: {

				current: null,

				data: {
					'0': function(){
						self.viewPort.current = 'mobile';
						self.fullpage.destroy();
						self.fsRem.stop();
						if ( $('.nav-item.active').size() ){
							$('.section_' + $('.nav-item.active').attr('data-menuanchor')).addClass('active');
						} else {
							//$('.nav-item').eq(0).addClass('active');
							$('.section').eq(0).addClass('active');
						}

						self.firstScreenHeight.setHeight();
					},
					'1360': function(){
						$('section.section.active').removeClass('active');
						self.viewPort.current = 'desktop';
						self.fullpage.init();
						self.fsRem.init();

						self.asideMenu.hide();

						$('.popup_order').removeClass('step-map step-1 step-2').addClass('step-1');
						self.firstScreenHeight.cleanHeight();
					}
				},

				init: function(data){
					var points = data || self.viewPort.data;
					if ( points ){
						points['Infinity'] = null;
						var sbw = scrollBarWidth(), curPoint = null;
						var ww = $(window).width() + sbw;
						checkCurrentViewport(ww);
						$(window).on('resize', function(){
							ww = $(window).width() + sbw;
							checkCurrentViewport(ww);
						});
					}

					function checkCurrentViewport(ww){
						var pp = 0, pc = null;
						$.each(points, function(point, callback){
							if ( point > ww ){
								if ( pp !== curPoint ) {
									curPoint = pp;
									pc();
								}
								return false;
							}
							pp = point; pc = callback;
						});
					}

					function scrollBarWidth(){
						var scrollDiv = document.createElement('div');
						scrollDiv.className = 'scroll_bar_measure';
						$(scrollDiv).css({
							width: '100px',
							height: '100px',
							overflow: 'scroll',
							position: 'absolute',
							top: '-9999px'
						});
						document.body.appendChild(scrollDiv);
						sbw = scrollDiv.offsetWidth - scrollDiv.clientWidth;
						document.body.removeChild(scrollDiv);
						return sbw;
					}

				}
			},

			rangeSlider: {

				convertValToTime: function(val) {
					var hours = parseInt(val / 60);
					var minutes = Math.floor(val % 60);
					var days = parseInt(minutes / 60);
					hours = hours % 60;
					if(String(hours).length != 2) hours = "0" + hours;
					if(String(minutes).length != 2) minutes = "0" + minutes;
					if( days == 0 ) { return hours + ":" + minutes; }
					return days + ":" + hours + ":" + minutes;
				},

				init: function(){

					var $slider = $('.slider-path');

					$slider.each(function(){
						var $t = $(this),
							slider = $t[0],
							rangeStep = 30;

						$t.find('.slider-path__min').text(self.rangeSlider.convertValToTime($t.data('range-min')));
						$t.find('.slider-path__max').text(self.rangeSlider.convertValToTime($t.data('range-max')));

						noUiSlider.create(slider, {
							start: [$t.data('val-min'), $t.data('val-max')],
							margin: 60,
							step: 30,
							animate: true,
							connect: true,
							//step: rangeStep,
							range: {
								'min': $t.data('range-min'),
								'max': $t.data('range-max')
							}
						});

						slider.noUiSlider.on('change', function ( values, handle ) {
							var val = values;
							val[handle] = (Math.round(values[handle] / rangeStep)) * rangeStep;
							slider.noUiSlider.set(val);
							$t.parent().find('.i-from').val(val[0]);
							$t.parent().find('.i-to').val(val[1]);

							setValue($t, values);
						});

						slider.noUiSlider.on('slide', function ( values, handle ) {
							setValue($t, values);
						});

						setValue($t, [$t.data('val-min'), $t.data('val-max')]);

					});

					function setValue($slider, val){
						for (var i = 0; i < val.length; i++){
							$slider.find('.noUi-handle[data-handle=' +i + ']').text(self.rangeSlider.convertValToTime(val[i]));
						}
					}

				}
			},

			fsRem: {

				data: {
					init: false,
					fs: 100,
					minWidth: 1360,
					minHeight: 600,
					currentWidth: 1920,
					currentHeight: 920,
					screenWidth: 0,
					screenHeight: 0,
					timer: null
				},

				calc: function(){
					var $screen = $(window);
					var $html = $('html');
					var multiplier, newFontSize;

					self.fsRem.data.screenHeight = $screen.height() >= self.fsRem.data.minHeight ? $screen.height() : self.fsRem.data.minHeight;
					self.fsRem.data.screenWidth = $screen.width() >= self.fsRem.data.minWidth ? $screen.width() : self.fsRem.data.minWidth;

					if ( self.fsRem.data.screenWidth/self.fsRem.data.screenHeight <= self.fsRem.data.currentWidth/self.fsRem.data.currentHeight ){
						multiplier = self.fsRem.data.screenWidth / self.fsRem.data.currentWidth;
						newFontSize = multiplier * self.fsRem.data.fs;
					} else {
						multiplier = self.fsRem.data.screenHeight / self.fsRem.data.currentHeight;
						newFontSize = multiplier * self.fsRem.data.fs;
					}

					$html.css({fontSize: newFontSize+'%'});

				},

				init: function(){
					self.fsRem.data.init = true;
					var $screen = $(window);
					self.fsRem.calc();
					$screen.resize(function(){
						if ( self.fsRem.data.timer ) clearTimeout(self.fsRem.data.timer);
						if ( self.fsRem.data.init ) self.fsRem.data.timer = setTimeout(self.fsRem.calc, 200);
					});
				},

				stop: function(){
					self.fsRem.data.init = false;
					clearTimeout(self.fsRem.data.timer);
					self.fsRem.data.timer = null;
					$('html').removeAttr('style');
				}
			},

			firstScreenHeight: {
				setHeight: function(){
					var height = $(document).width() * ( $(window).height() / $(window).width());
					var minus = $('.header').height() + $('.section_welcome .section__inner').height() + 20;
					$('.section_welcome .section__inner').css('padding-top', height - minus);
				},

				cleanHeight: function(){
					$('.section_welcome .section__inner').removeAttr('style');
				}

			},

			fullpage: {

				active: false,

				destroy: function(){

					if ( !self.fullpage.active ) return false;
					self.fullpage.active = false;
					$.fn.fullpage.destroy('all');
				},

				init: function(){

					if ( !!self.fullpage.active ) return false;

					self.fullpage.active = true;

					var scrollLock = false;

					var fpOptions = {
						verticalCentered: false,
						scrollingSpeed: 500,
						touchSensitivity: 30,
						anchors:[
							'welcome',
							'balance',
							'example',
							'calculate',
							'feedback',
							'team',
							'instagram',
							'feature',
							'faq'
						],
						menu: '.nav-list',
						onLeave: function(index, nextIndex, direction){
							if ( !scrollLock ){
								try{$.fn.fullpage.setAllowScrolling(false);} catch (e){}
								scrollLock = true;

								setTimeout(function(){
									try{$.fn.fullpage.setAllowScrolling(true);} catch (e){}
									scrollLock = false;
								}, 300);
							}

						}
					};

					$('.website').fullpage(fpOptions);
				}
			}

		},
		init: function() {

			//self.viewPort.init();

			setTimeout(function(){
				$('body').addClass('loaded');
			}, 200);

			$('.team-person_bg').each(function(){
				var parallax = new Parallax(this);
			});

			if ( window.isMobile ){
				$('html').addClass('is-mobile');
				self.viewPort.current = 'mobile';
				self.firstScreenHeight.setHeight();
			} else {
				$('html').addClass('is-desktop');
				self.viewPort.current = 'desktop';
				self.fullpage.init();
				self.fsRem.init();
			}

			self.iosFix.init();
			self.order.init();
			self.rangeSlider.init();
			self.calculator.init();
			self.feedbackSlider.init();

			$('[data-required=phone]').mask('+7 (999) 999 99 99');



			if ( self.viewPort.current == 'mobile' ) {
				if ( location.hash ){
					setTimeout(function(){
						$('.nav-item__link[href = ' + location.hash + ']').click();
					}, 0);
				}
			}

			//feedback-list

			$('.feedback-list').each(function () {
				var $t = $(this);
				swipeSlide($t, function(x){
					if ( x < 0){
						$t.closest('.feedback-carousel').find('.feedback-control__item_prev').click();
					} else if ( x > 0){
						$t.closest('.feedback-carousel').find('.feedback-control__item_next').click();
					}
				})
			});

			$('.nav').each(function () {
				var $t = $(this);
				swipeSlide($t, function(dx, x){
					if ( dx < 0){
						$('.header-handler').click();
					}
				})
			});

			$('.website').each(function () {
				var $t = $(this);
				swipeSlide($t, function(dx, x, x1){
					if ( x < 200 && dx > 0 && x1 > 100 ){
						$('.header-handler').click();
					}
				})
			});

			$('.team').each(function () {
				var $t = $(this);

				swipeSlide($t, function(dx, x){
					var $link = $('.team-item'),
						$linkActive = $link.filter('.active'),
						ind = $linkActive.index();

					if ( dx < 0){
						if ( ind < 0 ){
							$link.eq($link.length-1).click();
						} else {
							$link.eq((ind-1)).click();
						}
					} else if ( dx > 0){
						$link.eq((ind+1)%$link.length).click();
					}
				})
			});

			$('.balance').each(function () {
				var $t = $(this);

				swipeSlide($t, function(dx, x){
					var $link = $t.find('.tabs-item'),
						$linkActive = $link.filter('.active'),
						ind = $linkActive.index();

					if ( dx < 0){
						if ( ind < 0 ){
							$link.eq($link.length-1).click();
						} else {
							$link.eq((ind-1)).click();
						}
					} else if ( dx > 0){
						$link.eq((ind+1)%$link.length).click();
					}
				})
			});


			var $b = $('body');

			$b
				.on('click', '.a', function(e){
					e.preventDefault();
				})
				.on('click', '.header-handler', function(e){
					e.preventDefault();

					if (!$('html').hasClass('_menu')){
						self.asideMenu.show();
					} else {
						self.asideMenu.hide();
					}

				})
				.on('click', '.js-toggle-form', function(e){
					e.preventDefault();
					$(this)
						.hide()
						.siblings('form')
						.slideDown('fast');
				})
				.on('click', '.header-logo', function(e){
					e.preventDefault();
					if ( self.viewPort.current == 'mobile' ) {
						$('.nav-item').removeClass('active');
						$('.section')
							.removeClass('active')
							.eq(0)
							.addClass('active');
						$('html,body').animate({scrollTop:0}, 0);
						self.asideMenu.hide();
					}
				})
				.on('click', '.scrollto', function(e){
					e.preventDefault();
					var $this = $(this.hash);
					$('html,body').animate({scrollTop:$this.offset().top}, 500);
				})
				.on('click', '.nav-item__link', function(e){
					var $t = $(this),
						$item = $t.closest('.nav-item'),
						$body = $('body');

					self.popup.close('.popup');

					if ( self.viewPort.current == 'desktop' ) {
						$('.wrapper').removeClass('wrapper_showfoot');
						try{$.fn.fullpage.setAllowScrolling(true);} catch (e){}
					} else if ( self.viewPort.current == 'mobile' ){
						$item
							.addClass('active')
							.siblings()
							.removeClass('active');
						$('.section').removeClass('active');
						$('.section_' + $item.attr('data-menuanchor')).addClass('active');
						self.asideMenu.hide();
						//$('html,body').animate({scrollTop:0}, 300);

						$('html,body').animate({scrollTop: $('[data-scroll-id=' + $item.attr('data-menuanchor') + ']').offset().top}, 0);

						//setTimeout(function(){
						//	self.feedbackSlider.init();
						//}, 0)


						$body.removeClassWild('clr_*').addClass('clr_' + $item.attr('data-menuanchor'));

					}

				})
				.on('click', '.js-open-call', function(e){
					e.preventDefault();
					$('.wrapper').addClass('show-call');
				})
				.on('click', '.js-close-call', function(e){
					e.preventDefault();
					$('.wrapper').removeClass('show-call');
				})
				.on('click', '.js-open-popup', function(e){
					e.preventDefault();
					self.popup.close('.popup');
					self.popup.open($(this).attr('href'));
				})
				// popup close
				.on('click', '.popup', function(e){
					if ( !$(e.target).closest('.popup-layout').size() ) self.popup.close('.popup');
				})
				.on('click', '.popup-close', function(e){
					e.preventDefault();
					self.popup.close('.popup');
				})

				.on('click', '[data-tab-link]', function(e){
					e.preventDefault();
					var $t = $(this);
					var group = $t.data('tab-group');
					var $links = $('[data-tab-link]').filter(selectGroup);
					var $tabs = $('[data-tab-targ]').filter(selectGroup);
					var ind = $t.data('tab-link');
					var $tabItem = $('[data-tab-targ='+ind+']').filter(selectGroup);

					if( !$t.hasClass('active')){
						$links.removeClass('active');
						$t.addClass('active');

						$tabs.fadeOut(150);
						setTimeout(function(){
							$tabs.removeClass('active');

							$tabItem.fadeIn(150, function(){
								$(this).addClass('active');
							});

							self.feedbackSlider.init();

						}, 150)
					}


					function selectGroup(){
						return $(this).data('tab-group') === group;
					}

				})
				.on('submit', '.js-form', function(e){
					e.preventDefault();
					var $form = $(this);

					if ( self.checkForm($form) ){

						var data = $form.serialize();

						$form.find('[data-name]').each(function(){
							var $t = $(this);
							if ( $t.hasClass('active') ) {
								data += '&' + $t.data('name') + '=' + $t.data('value');
							}
						});

						$.ajax({
							type: $form.attr('method'),
							url: $form.attr('action'),
							data: data,
							complete: function(res){

								var data = JSON.parse(res.responseText);

								if ( res.status ){
									var status = res.status,
										formName = $form.data('form-name');

									if ( status == 200 ){	/** success */

										if ( data.title || data.message ) self.popup.info(data.title, data.message);

										switch (formName){
											case 'feedback':
												break;

											case 'faq':
												$form.find('input[type=text], textarea').val('');
												break;
										}

									} else if ( status == 400 ){	/** error */

									} else if ( status == 500 ) {	/** Internal Server Error */

									} else {	/** other trouble */
									console.error(res);
									}

								}
							}
						});
					}

				})
				.on('submit', '.js-send-ajax', function(e){
					e.preventDefault();
					var $form = (this);

					if ( self.checkForm($form) ){
						$form.submit();
					}

				})
				.on('click', '.js-load-day-menu', function(e){
					e.preventDefault();
					var $t = $(this);
					var url='/data/day_menu.json';
					var data = JSON.parse(JSON.stringify(eval("(" + $t.attr('data-storage') + ")")));
					var $content = $t.closest('.example').find('.example-set');
					var i = 0;

					$.ajax({
						type: 'post',
						url: url,
						data: data,
						success: function (data) {
							$t.addClass('active').siblings().removeClass('active');
							//console.info(data.menu.length);
							$content.fadeOut(150, function(){
								$(this).html('');

								for(i; i < data.menu.length; i++){
									$content.append(menuToHTML(data.menu[i]));
								}
								$(this).fadeIn(150)
							});

						},
						error: function(data) {
							console.error(data);
						}
					});

				})
				.mousewheel(function(e) {

					if ( self.viewPort.current == 'desktop' && !$('.popup').is(':visible') ) {

						var $scr = $(e.target).closest('.section_faq'),
							$wpr = $('.wrapper'),
							pause = 300;

						if ( $scr.size() && $scr.hasClass('fp-completely') && !$wpr.hasClass('wrapper_showfoot')){
							//console.info(e.deltaX, e.deltaY, e.deltaFactor);

							if ( e.deltaY < 0 ){
								try{$.fn.fullpage.setAllowScrolling(false);} catch (e){}
								$wpr.addClass('wrapper_showfoot');
							}
						}

						if ( $wpr.hasClass('wrapper_showfoot') && e.deltaY > 0 ){
							$wpr.removeClass('wrapper_showfoot');
							lock = true;
							setTimeout(function(){
								try{$.fn.fullpage.setAllowScrolling(true);} catch (e){}
							}, pause);
						}
					}
				})

			;


			$(window).on('scroll', function(){
				mobileMenuScrollSinch();
			});


			function mobileMenuScrollSinch(){
				var $anchor = $('.section__anchor'), $win = $(window), dim = {};

				dim.h = $win.height();
				dim.t = $win.scrollTop();

				//console.info(dim.h, dim.t);

				$anchor.each(function(){
					var $t = $(this), name = $t.attr('data-scroll-id'), $section = $t.closest('.section'), $body = $('body');

					if ( $t.offset().top <= dim.t && ( $t.offset().top + $t.height()) > ( dim.h + dim.t ) && !$section.hasClass('active') ){

					$('.nav-item').removeClass('active');
					$('[data-menuanchor=' + name + ']').addClass('active');

						$section
							.addClass('active')
							.siblings('.section')
							.removeClass('active')
							.find('.section__anchor')
							.removeClass('active');

						$body.removeClassWild('clr_*').addClass('clr_'+name);
					}
				});
			}

			function menuToHTML(data){
				var weight = data.weight ? '<div class="example-set__label">' + data.weight + '</div>' : '';
				return '<div class="example-set__item">' +
					'<div class="example-set__img"><img src="' + data.img + '" alt="' + data.title + '">' + weight + '</div>' +
					'<div class="example-set__title">' + data.title + '</div>' +
					'<div class="example-set__text">' + data.text + '</div>' +
					'</div>';
			}


			function swipeSlide( obj, callback ){
				var x = 0, x1 = 0, dx = 0, MinPath = 300;

				$(obj).on('touchstart', obj, function(e){
					x = e.pageX || e.originalEvent.targetTouches[0].pageX;
				});

				$('body').on('touchmove', obj, function(e){
					if ( x !== 0){
						x1 = e.pageX || e.originalEvent.targetTouches[0].pageX;
						dx = (( x1 - x ) > MinPath ) ? (1) : ( ( x1 - x ) < -MinPath ? (-1) : '0');
					}
				}).on('touchend', function(e){
					if ( x != 0 ){
						if ( callback )	callback(dx, x, x1);
					}
					x = 0;
					x1 = 0;
				});
			}

			$.fn.removeClassWild = function(mask) {
				return this.removeClass(function(index, cls) {
					var re = mask.replace(/\*/g, '\\S+');
					return (cls.match(new RegExp('\\b' + re + '', 'g')) || []).join(' ');
				});
			};

			$.urlParam = function (name) {
				var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
				if (results == null) { return null; }
				else { return results[1] || 0; }
			};


			if ($.urlParam('showPopup')) {
				var popupName = $.urlParam('showPopup');
				setTimeout(self.popup.open(popupName));
			}


		}
	};
	var self = {};
	var loader = function(){
		self = app.module;
		jQuery.app = app.module;
		app.init();
	};
	var ali = setInterval(function(){
		if (typeof jQuery !== 'function') return;
		clearInterval(ali);
		setTimeout(loader, 0);
	}, 50);

})();



