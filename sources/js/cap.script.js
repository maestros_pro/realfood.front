(function(){
	var app = {
		module: {

			checkForm: function(form){
				var $el = $(form),
					wrong = false;

				$el.find('[data-required]').each(function(){
					var $t = $(this),
						type = $t.data('required'),
						$wrap = $t.closest('.form-field'),
						val = $.trim($t.val()),
						rexp = /.+$/igm;

					$wrap.removeClass('error').removeClass('success');

					switch (type) {
						case 'number':
							rexp = /^\d+$/i;
							break;
						case 'phone':
							rexp = /[\+]\d\s[\(]\d{3}[\)]\s\d{3}\s\d{2}\s\d{2}/i;
							break;
						case 'letter':
							rexp = /^[A-zА-яЁё]+$/i;
							break;
						case 'rus':
							rexp = /^[А-яЁё]+$/i;
							break;
						case 'email':
							rexp = /^[-._a-z0-9]+@(?:[a-z0-9][-a-z0-9]+\.)+[a-z]{2,6}$/i;
							break;
						case 'password':
							rexp = /^(?=^.{8,}$)(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).*$/;
							break;
						default:
							rexp = /.+$/igm;
					}

					if ( !rexp.test(val) || val == 'false' || !val ){
						wrong = true;
						$wrap.addClass('error');

					} else {
						$wrap.addClass('success');
					}

				});

				return !wrong;

			},

			viewPort: {

				current: '',

				data: {
					'0': function(){
						self.viewPort.current = 'mobile';
						self.scrollBar.destroy();
					},
					'1360': function(){
						self.viewPort.current = 'desktop';
						self.scrollBar.init();
					}
				},

				init: function(data){
					var points = data || self.viewPort.data;
					if ( points ){
						points['Infinity'] = null;
						var
							//sbw = scrollBarWidth(),
							sbw = 0,
							curPoint = null,
							ww = $(window).width() + sbw;
						checkCurrentViewport(ww);

						$(window).on('resize', function(){
							ww = $(window).width() + sbw;
							checkCurrentViewport(ww);
						});
					}

					function checkCurrentViewport(ww){
						var pp = 0, pc = null;
						$.each(points, function(point, callback){
							if ( point > ww ){
								if ( pp !== curPoint ) {
									curPoint = pp;
									pc();
								}
								return false;
							}
							pp = point; pc = callback;
						});
					}

					function scrollBarWidth(){
						var scrollDiv = document.createElement('div');
						scrollDiv.className = 'scroll_bar_measure';
						$(scrollDiv).css({
							width: '100px',
							height: '100px',
							overflow: 'scroll',
							position: 'absolute',
							top: '-9999px'
						});
						document.body.appendChild(scrollDiv);
						sbw = scrollDiv.offsetWidth - scrollDiv.clientWidth;
						document.body.removeChild(scrollDiv);
						return sbw;
					}

				}
			},

			scrollBar: {

				destroy: function(){
					$('.js-scroll-inited').removeClass('js-scroll-inited').mCustomScrollbar('destroy');
				},

				init: function(){
					$('.js-scroll-wrap').not('.js-scroll-inited').mCustomScrollbar({
						mouseWheelPixels: 300,
						callbacks:{
							onCreate: function(){
								$(this).addClass('js-scroll-inited');
							}
						}
					});
				}
			}

		},

		init: function() {

			self.viewPort.init();

			$('.cap__carousel-list').slick({

				dots: false,
				infinite: true,
				speed: 300,
				slidesToShow: 6,
				swipe: true,
				touchMove: false,
				responsive: [
					{
						breakpoint: 1366,
						settings: {
							slidesToShow: 1
						}
					}
				]
			});

			var $b = $('body');

			$b

				.on('submit', '.js-subscribe-ajax', function(e){
					e.preventDefault();
					var $form = $(this);

					if ( !self.checkForm($form) ) return false;

					$.ajax({
						url: $form.attr('action'),
						method: $form.attr('method'),
						data: $form.serialize()
					}).success(function (data) {
						//console.info(data);

						if ( data.success == true ){
							$form.hide().next('.cap-form__send').show();
						}

					}).complete(function (xhr, textStatus) {
						//console.info(xhr);

					});

				})

				.on('click', '.js-show-rules', function(e){
					e.preventDefault();
					$b.addClass('_showrules');

				})

				.on('click', '.js-hide-rules', function(e){
					e.preventDefault();
					$b.removeClass('_showrules');

				})

				.on('focus', '.form-item_placeholder input', function(e){

					var $t = $(this);
					$t.closest('.form-item_placeholder').addClass('is-onfocus')
				})

				.on('blur', '.form-item_placeholder input', function(e){
					var $t = $(this);
					$t.closest('.form-item_placeholder').removeClass('is-onfocus')
				})

				.on('keydown', '.form-item_placeholder input', function(e){
					var $t = $(this);

					if (!$t.val()) {
						$t.closest('.form-item_placeholder').removeClass('is-filled')
					} else {
						$t.closest('.form-item_placeholder').addClass('is-filled')
					}
				})


			;


		}
	};
	var self = {};
	var loader = function(){
		self = app.module;
		jQuery.app = app.module;
		app.init();
	};
	var ali = setInterval(function(){
		if (typeof jQuery !== 'function') return;
		clearInterval(ali);
		setTimeout(loader, 0);
	}, 50);

})();



