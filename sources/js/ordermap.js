
$(function(){
	var myMap;

	ymaps.ready(function () {
		myMap = new ymaps.Map('orderMap', {
			center: [48.737136, 9.327072],
			zoom: 10,
			controls: []
		});

		myMapMob = new ymaps.Map('orderMapMob', {
			center: [48.737136, 9.327072],
			zoom: 10,
			controls: []
		})


	});


	$('#orderMap').resize(function(){
		myMap.container.fitToViewport();
	});

	$('#orderMapMob').resize(function(){
		myMapMob.container.fitToViewport();
	});

	//

});
